package com.levi9.student;

/**
 * Created by sergii on 11/3/14.
 */

import java.util.Arrays;

/**
 * Represents play field
 */
public class FieldProcessorImpl implements FieldProcessor{
    private char[][] canvas = new char[][]{
            {'.', '.', '.'},
            {'.', '.', '.'},
            {'.', '.', '.'}
    };
    private boolean finished;

    public char[][] getCanvas() {
        return canvas;
    }

    public void setCanvas(char[][] canvas) {
        this.canvas = canvas;
    }

    public char[][] setYourElement(int row, int column, char element) {
        canvas[row][column] = element;
        return canvas;
    }

    /**
     * check if player occupy vertical, horizontal or diagonal
     *
     * @param yourSign
     * @return
     */
    public boolean isWon(int yourSign) {
        if (canvas[0][0] == yourSign && canvas[0][1] == yourSign
                && canvas[0][2] == yourSign) {
            return true;
        }
        if (canvas[1][0] == yourSign && canvas[1][1] == yourSign
                && canvas[1][2] == yourSign) {
            return true;
        }
        if (canvas[2][0] == yourSign && canvas[2][1] == yourSign
                && canvas[2][2] == yourSign) {
            return true;
        }
        if (canvas[0][0] == yourSign && canvas[1][0] == yourSign
                && canvas[2][0] == yourSign) {
            return true;
        }
        if (canvas[0][1] == yourSign && canvas[1][1] == yourSign
                && canvas[2][1] == yourSign) {
            return true;
        }
        if (canvas[0][2] == yourSign && canvas[1][2] == yourSign
                && canvas[2][2] == yourSign) {
            return true;
        }
        if (canvas[0][0] == yourSign && canvas[1][1] == yourSign
                && canvas[2][2] == yourSign) {
            return true;
        }
        if (canvas[0][2] == yourSign && canvas[1][1] == yourSign
                && canvas[2][0] == yourSign) {
            return true;
        }
        return false;
    }

    @Override
    public String toString() {
        return Arrays.deepToString(canvas).replace("[[", "[").replace("]]", "]").replace("], ", "]\n") + "\n";
    }

    public boolean isFinished() {
        return finished;
    }

    public void setFinished(boolean finished) {
        this.finished = finished;
    }

    public boolean isFull() {
        for (char[] cnv : canvas) {
            for (int j = 0; j < cnv.length; j++) {
                // if at least one dot is on board
                if (cnv[j] == '.')
                    return false;
            }
        }
        return true;
    }

    public GameStatus getStatus() {
        if (finished) {
            return GameStatus.FINISHED;
        } else if (!finished && isFull()) {
            return GameStatus.DRAW;
        } else {
            return GameStatus.IN_PROGRESS;
        }
    }
}
