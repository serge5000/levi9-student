package com.levi9.student.semaphore;

// Import the necessary Java synchronization and scheduling classes.

import com.levi9.student.FieldProcessorImpl;
import com.levi9.student.player.Player;
import com.levi9.student.player.PlayerImpl;
import com.levi9.student.threads.PlayPingPongThread;

import java.util.concurrent.CountDownLatch;

/**
 * @class PingPongRight
 * @brief This class implements a Java program that creates two
 * instances of the PlayPingPongThread and start these thread
 * instances to correctly alternate printing "Ping" and "Pong",
 * respectively, on the console display.
 */
public class PingPongRight {
    public static FieldProcessorImpl field = new FieldProcessorImpl();
    /**
     * Number of iterations to run the test program.
     */
    public final static int mMaxIterations = 5;

    /**
     * Latch that will be decremented each time a thread exits.
     */
    public static CountDownLatch mLatch = null;

    /**
     * @class PlayPingPongThread
     *
     * @brief This class implements the ping/pong processing algorithm
     *        using the SimpleSemaphore to alternate printing "ping"
     *        and "pong" to the console display.
     */


    /**
     * The method that actually runs the ping/pong program.
     */
    public static void process(String startString,
                               String finishString,
                               int maxIterations) throws InterruptedException {

        // constructor call.
        mLatch = new CountDownLatch(2);

        // Create the ping and pong SimpleSemaphores that control
        // alternation between threads.

        //make pingSema start out unlocked.
        SimpleSemaphore pingSema = new SimpleSemaphore(1, true);
        //make pongSema start out locked.
        SimpleSemaphore pongSema = new SimpleSemaphore(0, true);
        //
        Player playerOne = new PlayerImpl('x', field);
        Player playerTwo = new PlayerImpl('0', field);

        System.out.println(startString);

        // Create the ping and pong threads, passing in the string to
        // print and the appropriate SimpleSemaphores.
        PlayPingPongThread ping = new PlayPingPongThread(pingSema, pongSema, playerOne, maxIterations, field);
        PlayPingPongThread pong = new PlayPingPongThread(pongSema, pingSema, playerTwo, maxIterations, field);

        //Initiate the ping and pong threads, which will call
        new Thread(ping).start();
        new Thread(pong).start();

        mLatch.await();
        // synchronizer call to mLatch that waits for both threads to
        // finish.
        //throw new InterruptedException();

        System.out.println(finishString);
    }

    /**
     * The main() entry point method into PingPongRight program.
     *
     * @throws InterruptedException
     */
    public static void main(String[] args) throws InterruptedException {
        process("Ready...Set...Go!",
                "Done!",
                mMaxIterations);
    }
}
