package com.levi9.student.semaphore;

import java.util.concurrent.locks.Condition;
import java.util.concurrent.locks.ReentrantLock;

/**
 * @class SimpleSemaphore
 * @brief This class provides a simple counting semaphore implementation using
 * Java a ReentrantLock and a ConditionObject (which is accessed via a
 * Condition). It must implement both "Fair" and "NonFair" semaphore
 * semantics, just liked Java Semaphores.
 */
public class SimpleSemaphore {
    /**
     * Define a ReentrantLock to protect the critical section.
     */
    private ReentrantLock lock;

    /**
     * Define a Condition that waits while the number of permits is 0.
     */
    private Condition condition;

    /**
     * Define a count of the number of available permits.
     */
    private int permits = 0;
    // ensure its values aren't cached by multiple Threads..

    public SimpleSemaphore(int permits, boolean fair) {
        this.permits = permits;
        this.lock = new ReentrantLock(fair);
        condition = lock.newCondition();
    }

    /**
     * Acquire one permit from the semaphore in a manner that can be
     * interrupted.
     */
    public void acquire() throws InterruptedException {
        lock.lockInterruptibly();
        try {
            while (permits == 0)
                condition.await();
            --permits;
        } finally {
            lock.unlock();
        }
    }

    /**
     * Acquire one permit from the semaphore in a manner that cannot be
     * interrupted.
     */
    public void acquireUninterruptibly() {
        lock.lock();
        try {
            while (permits == 0)
                condition.awaitUninterruptibly();
            --permits;
        } finally {
            lock.unlock();
        }
    }

    /**
     * Return one permit to the semaphore.
     */
    public void release() {
        lock.lock();
        ++permits;
        condition.signal();
        lock.unlock();
    }

    /**
     * Return the number of permits available.
     */
    public int availablePermits() {
        // TODO - you fill in here to return the correct result
        return permits;
    }
}
