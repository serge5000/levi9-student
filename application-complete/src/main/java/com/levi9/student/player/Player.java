package com.levi9.student.player;

/**
 * Created by sergii on 11/7/14.
 */
public interface Player {
    char[][] yourTurn();

    boolean isMiddleEmpty(char[][] canvas);

    boolean isWon();

    char getSign();
}
