package com.levi9.student.player;

import com.levi9.student.FieldProcessorImpl;

import java.util.Random;

/**
 * Created by s.getman on 11/4/2014.
 */
public class PlayerImpl implements Player {
    private char yourSign;
    private FieldProcessorImpl field;

    public PlayerImpl(char sign, FieldProcessorImpl field) {
        yourSign = sign;
        this.field = field;
    }

    /**
     * method
     *
     * @return
     */
    public char[][] yourTurn() {
        if (!field.isFinished()) {
            if (isMiddleEmpty(field.getCanvas())) {
                field.getCanvas()[1][1] = yourSign;
            } else {
                int row = new Random().nextInt(3);
                int column = new Random().nextInt(3);
                while (!(field.getCanvas()[row][column] == '.')) {
                    row = new Random().nextInt(3);
                    column = new Random().nextInt(3);
                }

                field.setYourElement(row, column, yourSign);
            }
        }
        return field.getCanvas();
    }


    public boolean isMiddleEmpty(char[][] canvas) {
        return canvas[1][1] == 0;
    }

    @Override
    public boolean isWon() {
        if (field.isWon(yourSign)) {
            field.setFinished(true);
            return true;
        }
        return false;
    }

    @Override
    public char getSign() {
        return yourSign;
    }
}
