package com.levi9.student;

/**
 * Created by sergii on 1/30/15.
 */
public interface FieldProcessor {
    char[][] getCanvas();

    void setCanvas(char[][] canvas);

    char[][] setYourElement(int row, int column, char element);
    
    boolean isWon(int yourSign);

    boolean isFinished();

    void setFinished(boolean finished);

    boolean isFull();

    GameStatus getStatus();
}
