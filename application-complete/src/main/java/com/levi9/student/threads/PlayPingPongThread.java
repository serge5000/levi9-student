package com.levi9.student.threads;

import com.levi9.student.FieldProcessor;
import com.levi9.student.FieldProcessorImpl;
import com.levi9.student.GameStatus;
import com.levi9.student.player.Player;
import com.levi9.student.semaphore.PingPongRight;
import com.levi9.student.semaphore.SimpleSemaphore;

/**
 * Created by suren on 1/28/15.
 */
public class PlayPingPongThread extends Thread {

    /**
     * Maximum number of loop iterations.
     */
    private int mMaxLoopIterations = 0;
    /**
     * Two SimpleSemaphores use to alternate pings and pongs.  You
     * can use an array of SimpleSemaphores or just define them as
     * two data members.
     */
    private final SimpleSemaphore semaphoreOne;
    private final SimpleSemaphore semaphoreTwo;
    private final Player player;
    private final FieldProcessor fieldProcessor;

    /**
     * Constructor initializes the data member(s).
     */
    public PlayPingPongThread(final SimpleSemaphore semaphoreOne,
                              final SimpleSemaphore semaphoreTwo,
                              final Player player,
                              final int maxIterations, final FieldProcessorImpl field) {
        this.player = player;
        this.mMaxLoopIterations = maxIterations;
        this.semaphoreOne = semaphoreOne;
        this.semaphoreTwo = semaphoreTwo;
        this.fieldProcessor = field;

    }

    /**
     * Main event loop that runs in a separate thread of control
     * and performs the ping/pong algorithm using the
     * SimpleSemaphores.
     */
    public void run() {
        for (int i = 1; i <= mMaxLoopIterations && !fieldProcessor.isFinished(); i++) {
            acquire();
            //player #x turn
            player.yourTurn();
            // need at least 3 steps to make any conclusions
            if (i > 3) {
                if (player.isWon()) {
                    System.out.println(fieldProcessor);
                    System.out.println(player.getSign() + " Won!");
                    release();
                    break;
                } else if (fieldProcessor.getStatus().equals(GameStatus.DRAW)) {
                    fieldProcessor.setFinished(true);
                    System.out.println(fieldProcessor);
                    System.out.println("Draw");
                }
            }
            release();
        }
        PingPongRight.mLatch.countDown();
    }

    /**
     * Method for acquiring the appropriate SimpleSemaphore.
     */
    private void acquire() {
        semaphoreOne.acquireUninterruptibly();
    }

    /**
     * Method for releasing the appropriate SimpleSemaphore.
     */
    private void release() {
        this.semaphoreTwo.release();
    }
}
