import com.levi9.student.FieldProcessor
import com.levi9.student.FieldProcessorImpl
import com.levi9.student.GameStatus
import com.levi9.student.player.PlayerImpl

/**
 * Created by suren on 1/30/15.
 */
class GroovyTest extends GroovyTestCase {

    void testElementB() {

        char[][] expected = [
                ['.', '.', '.'],
                ['.', '.', '.'],
                ['x', '.', '.']
        ]
        FieldProcessor field = new FieldProcessorImpl()
        char[][] actual = field.setYourElement(2, 0, 'x' as char)
        assert expected == actual
    }

    void testCheckYourPlayer() {

        final char[][] initial = [
                ['.', '.', '.'],
                ['.', '.', '.'],
                ['.', '.', '.']
        ]
        def player = new PlayerImpl('x' as char, new FieldProcessorImpl());


        char[][] turn = player.yourTurn()
        assert initial != turn
    }


    void testCheckGameStatusFinished() {
        final char[][] initial = [
                ['x', '.', '.'],
                ['0', 'x', '.'],
                ['0', '.', 'x']
        ]
        def field = new FieldProcessorImpl()
        field.setCanvas(initial);
        def yourPlayer = new PlayerImpl('x' as char, field)
        yourPlayer.isWon()
        assert GameStatus.FINISHED == field.getStatus()
    }


     void testCheckGameStatusDraw() {
        final char[][] initial = [
                ['x', '0', 'x'],
                ['0', '0', '0'],
                ['0', 'x', 'x']
        ]

        def field = new FieldProcessorImpl()
        field.setCanvas(initial)
        assert GameStatus.DRAW == field.getStatus()
    }


    void testCheckGameStatusInProgress() {
        final char[][] initial = [
                ['x', '.', '.'],
                ['0', 'x', '.'],
                ['0', '.', '.']
        ]
        def field = new FieldProcessorImpl();
        field.setCanvas(initial);

        assert GameStatus.IN_PROGRESS == field.getStatus()
    }
}