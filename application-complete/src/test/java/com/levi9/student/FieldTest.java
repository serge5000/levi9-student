package com.levi9.student;


import com.levi9.student.player.Player;
import com.levi9.student.player.PlayerImpl;
import mockit.Expectations;
import org.junit.Test;

import static org.junit.Assert.*;

public class FieldTest {

    @Test
    public void setElementLB() {
        char[][] expected = new char[][]{
                {'.', '.', '.'},
                {'.', '.', '.'},
                {'x', '.', '.'}
        };
        FieldProcessor field = new FieldProcessorImpl();
        char[][] actual = field.setYourElement(2, 0, 'x');
        assertArrayEquals(expected, actual);
    }

    @Test
    public void checkYourPlayer() {
        final char[][] initial = new char[][]{
                {'.', '.', '.'},
                {'.', '.', '.'},
                {'.', '.', '.'}
        };
        final Player yourPlayer = new PlayerImpl('x', new FieldProcessorImpl());

        new Expectations() {{
            yourPlayer.isMiddleEmpty(initial);
        }};

        char[][] turn = yourPlayer.yourTurn();
        assertNotSame(initial, turn);
    }

    @Test
    public void checkGameStatusFinished() {
        final char[][] initial = new char[][]{
                {'x', '.', '.'},
                {'0', 'x', '.'},
                {'0', '.', 'x'}
        };
        final FieldProcessor field = new FieldProcessorImpl();
        field.setCanvas(initial);
        final Player yourPlayer = new PlayerImpl('x', new FieldProcessorImpl());
        yourPlayer.isWon();

        new Expectations() {{
            field.setFinished(true);
        }};

        assertSame(GameStatus.FINISHED, field.getStatus());
    }

    @Test
    public void checkGameStatusDraw() {
        final char[][] initial = new char[][]{
                {'x', '0', 'x'},
                {'0', '0', '0'},
                {'0', 'x', 'x'}
        };
        FieldProcessor field = new FieldProcessorImpl();
        field.setCanvas(initial);

        assertSame(GameStatus.DRAW, field.getStatus());
    }

    @Test
    public void checkGameStatusInProgress() {
        final char[][] initial = new char[][]{
                {'x', '.', '.'},
                {'0', 'x', '.'},
                {'0', '.', '.'}
        };
        FieldProcessor field = new FieldProcessorImpl();
        field.setCanvas(initial);

        assertSame(GameStatus.IN_PROGRESS, field.getStatus());
    }

    /*@Test
    public void checkMiddle(){
        int[][] initial = new int[][]{
                { 0, 0, 0},
                { 0, 1, 0},
                { 0, 0, 0}
        };
        PlayerImpl yourPlayer = new PlayerImpl(1, new Field());
        int[][] turn = yourPlayer.yourTurn();
        assertArrayEquals(initial, turn);
    }

    @Test
    public void middleIsNotEmpty(){
        int[][] initial = new int[][]{
                { 0, 0, 0},
                { 0, 1, 0},
                { 0, 0, 0}
        };
        Field field = new Field();
        field.setYourElement(1,1,1);
        PlayerImpl yourPlayer = new PlayerImpl(1, field);
        int[][] turn = yourPlayer.yourTurn();
        assertNotSame(initial, turn);
    }

    @Test
    public void checkVictory(){
        int[][] initial = new int[][]{
                { 1, 0, 0},
                { 0, 1, 0},
                { 0, 0, 1}
        };
        Field field = new Field();
        field.setCanvas(initial);
        assertTrue(field.isWon(1));

        initial = new int[][]{
                {1, 1, 0},
                {0, 0, 0},
                {0, 0, 1}
        };
        field.setCanvas(initial);
        assertFalse(field.isWon(1));
    }*/
}