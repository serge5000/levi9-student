package com.levi9.student;

/**
 * Created by s.getman on 1/30/2015.
 */
public class UnimplementedException extends RuntimeException {
    public UnimplementedException(String message) {
        super(message);
    }
}
