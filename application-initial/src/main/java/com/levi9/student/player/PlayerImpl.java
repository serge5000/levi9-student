package com.levi9.student.player;

import com.levi9.student.FieldProcessor;
import com.levi9.student.UnimplementedException;

/**
 * Created by s.getman on 11/4/2014.
 */
public class PlayerImpl implements Player {
    private char yourSign;
    private FieldProcessor fieldProcessor;

    public PlayerImpl(char sign, FieldProcessor fieldProcessor) {
        yourSign = sign;
        this.fieldProcessor = fieldProcessor;
    }

    /**
     * method
     *
     * @return
     */
    public char[][] yourTurn() {
        throw new UnimplementedException("implement : Player.yourTurn");
    }


    public boolean isMiddleEmpty(char[][] canvas) {
        throw new UnimplementedException("implement : Player.isMiddleEmpty");
    }

    @Override
    public boolean isWon() {
        throw new UnimplementedException("implement : Player.isWon");
    }

    @Override
    public char getSign() {
        throw new UnimplementedException("implement : Player.getSign");
    }
}
