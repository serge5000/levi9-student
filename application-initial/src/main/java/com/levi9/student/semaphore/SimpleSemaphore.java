package com.levi9.student.semaphore;

import com.levi9.student.UnimplementedException;

import java.util.concurrent.locks.Condition;
import java.util.concurrent.locks.ReentrantLock;

/**
 * @class SimpleSemaphore
 * @brief This class provides a simple counting semaphore implementation using
 * Java a ReentrantLock and a ConditionObject (which is accessed via a
 * Condition). It must implement both "Fair" and "NonFair" semaphore
 * semantics, just liked Java Semaphores.
 */
public class SimpleSemaphore {
    /**
     * Define a ReentrantLock to protect the critical section.
     */
    private ReentrantLock lock;

    /**
     * Define a Condition that waits while the number of permits is 0.
     */
    private Condition condition;

    /**
     * Define a count of the number of available permits.
     */
    private int permits;
    // ensure its values aren't cached by multiple Threads..

    public SimpleSemaphore(int permits, boolean fair) {

    }

    /**
     * Acquire one permit from the semaphore in a manner that can be
     * interrupted.
     */
    public void acquire() throws InterruptedException {
        throw new UnimplementedException("implement SimpleSemaphore.acquire");
    }

    /**
     * Acquire one permit from the semaphore in a manner that cannot be
     * interrupted.
     */
    public void acquireUninterruptibly() {
        throw new UnimplementedException("implement SimpleSemaphore.acquireUninterruptibly");
    }

    /**
     * Return one permit to the semaphore.
     */
    public void release() {
        throw new UnimplementedException("implement SimpleSemaphore.release");
    }

    /**
     * Return the number of permits available.
     */
    public int availablePermits() {
        throw new UnimplementedException("implement SimpleSemaphore.release");
    }
}
