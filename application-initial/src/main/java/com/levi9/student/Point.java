package com.levi9.student;

/**
 * Created by sergii on 12/27/14.
 */

/**
 * Represents turn point
 */
public class Point {
    private int x;
    private int y;

    public int getX() {
        return x;
    }

    public void setX(int x) {
        this.x = x;
    }

    public int getY() {
        return y;
    }

    public void setY(int y) {
        this.y = y;
    }
}
