package com.levi9.student;

/**
 * Created by sergii on 11/3/14.
 */

import java.util.Arrays;

/**
 * Represents play field
 */
public class FieldProcessorImpl implements FieldProcessor{
    private char[][] canvas = new char[][]{
            {'.', '.', '.'},
            {'.', '.', '.'},
            {'.', '.', '.'}
    };
    private boolean finished;

    public char[][] getCanvas() {
        return canvas;
    }

    public void setCanvas(char[][] canvas) {
        this.canvas = canvas;
    }

    public char[][] setYourElement(int row, int column, char element) {
        //TODO: write your code heare
        return canvas;
    }

    /**
     * check if player occupy vertical, horizontal or diagonal
     *
     * @param yourSign
     * @return
     */
    public boolean isWon(int yourSign) {
        throw new UnimplementedException("implement FieldProcessor.isWon");
    }

    @Override
    public String toString() {
        return Arrays.deepToString(canvas).replace("[[", "[").replace("]]", "]").replace("], ", "]\n") + "\n";
    }

    public boolean isFinished() {
        return finished;
    }

    public void setFinished(boolean finished) {
        this.finished = finished;
    }

    public boolean isFull() {
        throw new UnimplementedException("implement FieldProcessor.isWon");
    }

    public GameStatus getStatus() {
        throw new UnimplementedException("implement FieldProcessor.isWon");
    }
}
