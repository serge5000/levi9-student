package com.levi9.student.threads;

import com.levi9.student.FieldProcessor;
import com.levi9.student.GameStatus;
import com.levi9.student.UnimplementedException;
import com.levi9.student.player.Player;
import com.levi9.student.semaphore.PingPongRight;
import com.levi9.student.semaphore.SimpleSemaphore;

/**
 * Created by suren on 1/28/15.
 */
public class PlayPingPongThread extends Thread {

    /**
     * Maximum number of loop iterations.
     */
    private int mMaxLoopIterations = 0;
    /**
     * Two SimpleSemaphores use to alternate pings and pongs.  You
     * can use an array of SimpleSemaphores or just define them as
     * two data members.
     */

    /**
     * Constructor initializes the data member(s).
     */
    public PlayPingPongThread(final SimpleSemaphore semaphoreOne,
                              final SimpleSemaphore semaphoreTwo,
                              final Player player,
                              final int maxIterations, final FieldProcessor fieldProcessor) {
    }

    /**
     * Main event loop that runs in a separate thread of control
     * and performs the ping/pong algorithm using the
     * SimpleSemaphores.
     */
    public void run() {
        throw new UnimplementedException("implement PlayPingPongThread.run");
    }

    /**
     * Method for acquiring the appropriate SimpleSemaphore.
     */
    private void acquire() {
        throw new UnimplementedException("implement PlayPingPongThread.run");
    }

    /**
     * Method for releasing the appropriate SimpleSemaphore.
     */
    private void release() {
        throw new UnimplementedException("implement PlayPingPongThread.run");
    }
}
