package com.levi9.student;

/**
 * Created by sergii on 1/29/15.
 */
public enum GameStatus {
    IN_PROGRESS, FINISHED, DRAW
}
